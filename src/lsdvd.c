
#include <glib.h>
#include "dvd.h"

static void
print_chapters (DVD *dvd, DVDTrack *track)
{
	GList *l;

	for (l = track->chapters; l != NULL; l = l->next)
	{
		Chapter *chap = l->data;
		g_print ("Chapter: %02d Length: %d\n", chap->index,
				(int) chap->length);
	}
}

static void
print_audio (DVD *dvd, DVDTrack *track)
{
	GList *l;

	for (l = track->audio; l != NULL; l = l->next)
	{
		Lang *lang = l->data;
		g_print ("Audio: %02d Lang: %s\n", lang->index, lang->lang);
	}
}

static void
print_subs (DVD *dvd, DVDTrack *track)
{
	GList *l;

	for (l = track->subtitle; l != NULL; l = l->next)
	{
		Lang *lang = l->data;
		g_print ("Subtitle: %02d Lang: %s\n", lang->index, lang->lang);
	}
}


int main (int argc, char **argv)
{
	DVD *dvd;
	GList *tracks, *l;

	dvd = dvd_new ("/dev/cdrom");
	if (dvd == NULL)
	{
		g_print ("Not a DVD, or couldn't open it\n");
		return 0;
	}

	g_print ("Title: %s\n", dvd_get_title_name (dvd));

	tracks = dvd_get_tracks (dvd);
	for (l = tracks; l != NULL; l = l->next)
	{
		DVDTrack *track = l->data;

		g_print ("\nTrack: %02d Length: %d\n", track->index,
				(int) track->length);
		g_print ("Chapters: %d Audio: %d Subtitles: %d\n",
				track->num_chapters, track->audio_langs,
				track->subtitle_langs);
		g_print ("Format: %s Width: %d Height: %d\n",
				track->format == FORMAT_NTSC ? "NTSC" : "PAL",
				track->width, track->height);

		g_print ("\n");
		print_chapters (dvd, track);
		print_audio (dvd, track);
		print_subs (dvd, track);
		g_print ("\n\n");
	}

	dvd_free (dvd);

	return 0;
}

