
#include "config.h"

#include <gnome.h>
#include <glade/glade.h>
#include <libgnomecanvas/gnome-canvas.h>
#include <libgnomecanvas/gnome-canvas-util.h>
#include <libgnomecanvas/gnome-canvas-pixbuf.h>

#include "crop-area.h"
#include "dvd.h"

struct LastMile {
	GladeXML *xml;
	GtkWidget *window;

	GtkTreeView *treeview;
};
typedef struct LastMile LastMile;

enum {
	NAME_COL,
	TIME_COL,
	TIME_STR_COL,
	NUM_COLS
};

static char *
lm_time_to_string (int time)
{
	int sec, min, hour;

	sec = time % 60;
	time = time - sec;
	min = (time % (60*60)) / 60;
	time = time - (min * 60);
	hour = time / (60*60);

	if (hour > 0)
	{
		/* hour:minutes:seconds */
		return g_strdup_printf ("%d:%02d:%02d", hour, min, sec);
	} else {
		/* minutes:seconds */
		return g_strdup_printf ("%d:%02d", min, sec);
	}

	return NULL;
}

static void
update_content_tracks_helper (GtkTreeStore *store, GtkTreeIter *parent,
		const char *name, int time)
{
	GtkTreeIter iter;
	char *time_str;

	time_str = lm_time_to_string (time);
	gtk_tree_store_append (store, &iter, parent);
	gtk_tree_store_set (store, &iter,
			NAME_COL, name,
			TIME_COL, time,
			TIME_STR_COL, time_str,
			-1);
	g_free (time_str);
}

static void
update_content_tracks (GtkTreeStore *store,
		GtkTreeIter *parent, DVDTrack *track)
{
	GList *l;

	/* Be intelligent about this, don't show chapters if there's
	 * only one, the user can select the whole track */
	if (track->num_chapters == 1)
		return;

	for (l = track->chapters; l != NULL; l = l->next)
	{
		Chapter *chap = l->data;
		char *name;

		name = g_strdup_printf (_("Chapter %02d"), chap->index);
		update_content_tracks_helper (store, parent, name, chap->length);
		g_free (name);
	}
}

static GtkTreeIter
update_content_helper (GtkTreeStore *store, const char *name, int time)
{
	GtkTreeIter iter;
	char *time_str;

	time_str = lm_time_to_string (time);
	gtk_tree_store_append (store, &iter, NULL);
	gtk_tree_store_set (store, &iter,
			NAME_COL, name,
			TIME_COL, time,
			TIME_STR_COL, time_str,
			-1);
	g_free (time_str);

	return iter;
}

static void
update_content (LastMile *lm)
{
	GtkTreeStore *store;
	DVD *dvd;
	char *title;
	GList *tracks, *l;

	store = GTK_TREE_STORE (gtk_tree_view_get_model (lm->treeview));
	gtk_tree_store_clear (store);

	//FIXME get from gconf
	dvd = dvd_new ("/dev/cdrom");

	if (dvd == NULL)
	{
		gtk_window_set_title (GTK_WINDOW (lm->window),
				_("Last Mile"));
		//FIXME warning if not on startup
		return;
	}

	title = g_strdup_printf (_("Last Mile - %s"),
			dvd_get_title_name (dvd));
	gtk_window_set_title (GTK_WINDOW (lm->window), title);
	g_free (title);

	tracks = dvd_get_tracks (dvd);

	if (tracks != NULL)
	{
		//FIXME update subs
		//FIXME update languages
	}

	for (l = tracks; l != NULL; l = l->next)
	{
		DVDTrack *track = l->data;
		GtkTreeIter iter;
		char *name;

		name = g_strdup_printf (_("Track %02d"), track->index);
		iter = update_content_helper (store, name, track->length);
		g_free (name);

		update_content_tracks (store, &iter, track);
	}

	dvd_free (dvd);
	gtk_tree_view_expand_all (lm->treeview);
}

static void
init_columns (GtkTreeView *treeview)
{
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;

	/* The name of the track/chapter */
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Track"),
			renderer,
			"text", NAME_COL,
			NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (treeview, column);

	/* Time */
	//FIXME make it right aligned
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Length"),
			renderer,
			"text", TIME_STR_COL,
			NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (treeview, column);
}

static void
init_treeview (LastMile *lm)
{
	GtkTreeModel *model;

	/* the model */
	model = GTK_TREE_MODEL (gtk_tree_store_new (NUM_COLS,
				G_TYPE_STRING,
				G_TYPE_INT,
				G_TYPE_STRING));

	/* the treeview */
	gtk_tree_view_set_model (lm->treeview, model);
	gtk_tree_view_set_rules_hint (lm->treeview, TRUE);
	g_object_unref (G_OBJECT (model));

	init_columns (lm->treeview);
	update_content (lm);
}

int main (int argc, char **argv)
{
	GladeXML *xml;
	LastMile *lm;

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	g_set_application_name (_("Last Mile DVD Ripper"));

	gnome_program_init ("lastmile", VERSION,
			LIBGNOMEUI_MODULE,
			argc, argv,
			GNOME_PARAM_APP_DATADIR, DATADIR,
			GNOME_PARAM_NONE);

	xml = glade_xml_new ("lastmile.glade", NULL, NULL);

	lm = g_new0 (LastMile, 1);
	lm->xml = xml;
	lm->window = glade_xml_get_widget (xml, "window");
	lm->treeview = GTK_TREE_VIEW (glade_xml_get_widget (xml, "treeview1"));

	crop_area_setup (xml);
	init_treeview (lm);
	gtk_widget_show_all (lm->window);

	gtk_main ();

	return 0;
}

