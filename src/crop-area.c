
#include "config.h"

#include <gnome.h>
#include <libgnomecanvas/gnome-canvas.h>
#include <libgnomecanvas/gnome-canvas-util.h>
#include <libgnomecanvas/gnome-canvas-pixbuf.h>

#include "crop-area.h"

#define COLOUR "blue"
#define THICKNESS 1

typedef enum {
	CROP_TOP,
	CROP_BOTTOM,
	CROP_LEFT,
	CROP_RIGHT,
	NUM_CROPS,
} CropTypes;

static char *spins[] = {
	"top_spin",
	"bottom_spin",
	"left_spin",
	"right_spin",
};

typedef struct CropArea CropArea;

struct CropArea {
	GnomeCanvas *canvas;

	GnomeCanvasItem *image;
	GnomeCanvasItem *top, *bottom, *left, *right;

	GladeXML *xml;

	int width, height;
};

static GnomeCanvasItem*
draw_a_line(GnomeCanvasGroup *group,
		int x1, int y1, int x2, int y2, char *color)
{
	GnomeCanvasPoints *points;
	GnomeCanvasItem *item;
	    
	points = gnome_canvas_points_new (2);

	points->coords[0] = x1;
	points->coords[1] = y1;
	points->coords[2] = x2;
	points->coords[3] = y2;
	item = gnome_canvas_item_new(group,
			gnome_canvas_line_get_type(),
			"points", points,
			"fill_color", color,
			"width_units", (double)THICKNESS,
			NULL);

	gnome_canvas_points_free(points);

	return item;
}

static CropArea*
lm_crop_area_new (GdkPixbuf *pix)
{
	CropArea *crop;
	GtkWidget *widget;
	GnomeCanvas *canvas;
	int w, h;

	crop = g_new0 (CropArea, 1);

	widget = gnome_canvas_new ();
	canvas = GNOME_CANVAS(widget);
	crop->canvas = canvas;

	w = gdk_pixbuf_get_width (pix);
	h = gdk_pixbuf_get_height (pix);
	crop->width = w;
	crop->height = h;

	crop->image = gnome_canvas_item_new (gnome_canvas_root(canvas),
			GNOME_TYPE_CANVAS_PIXBUF,
			"pixbuf", pix, NULL);

	gnome_canvas_set_scroll_region(canvas, 0.0, 0.0,
			(double)w + THICKNESS, (double)h + THICKNESS);
	gtk_widget_set_usize(widget, w, h);

	crop->top = draw_a_line (gnome_canvas_root(canvas),
			0, 0, w, 0, COLOUR);
	crop->bottom = draw_a_line (gnome_canvas_root(canvas),
			0, h, w, h, COLOUR);
	crop->right = draw_a_line (gnome_canvas_root(canvas),
			w, 0, w, h, COLOUR);
	crop->left = draw_a_line (gnome_canvas_root(canvas),
			0, 0, 0, h, COLOUR);

	return crop;
}

static void
value_changed_cb (GtkSpinButton *spinbutton, CropArea *crop)
{
	CropTypes type;
	int value, x, y;
	GnomeCanvasItem *item;

	type = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (spinbutton),
				"type"));
	value = gtk_spin_button_get_value (spinbutton);

	switch (type)
	{
	case CROP_TOP:
		x = 0;
		y = value;
		item = crop->top;
//		set_spins_range (crop->xml, crop, CROP_TOP, );
		break;
	case CROP_BOTTOM:
		x = 0;
		y = value - crop->height;
		item = crop->bottom;
		break;
	case CROP_LEFT:
		x = value;
		y = 0;
		item = crop->left;
		break;
	case CROP_RIGHT:
		x = value - crop->width;
		y = 0;
		item = crop->right;
		break;
	default:
		g_assert_not_reached ();
	}

	/* Do the actual movement */
	{
		double affine[6];
		art_affine_translate (affine, x, y);
		gnome_canvas_item_affine_absolute (item, affine);
	}

	switch (type)
	{
	case CROP_TOP:
	case CROP_BOTTOM:
		//set_spins_range (crop->xml, crop, CROP_TOP, );
		//set_spins_range (crop->xml, crop, CROP_TOP, );
	case CROP_LEFT:
	case CROP_RIGHT:
		//set_spins_range (crop->xml, crop, CROP_TOP, );
		//set_spins_range (crop->xml, crop, CROP_TOP, );
	}
}

static void
set_spins_range (GladeXML *xml, CropArea *crop,
		CropTypes type, int max, int cur)
{
	GtkWidget *item;
	int min = 0;

	item = glade_xml_get_widget (xml, spins[type]);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON (item), min, max);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (item), cur);
	g_object_set_data (G_OBJECT (item), "type", GINT_TO_POINTER (type));

	g_signal_connect (G_OBJECT (item), "value-changed",
			G_CALLBACK (value_changed_cb),
			crop);
}

void
crop_area_setup (GladeXML *xml)
{
	CropArea *crop;
	GdkPixbuf *pix;
	int w, h;

	pix = gdk_pixbuf_new_from_file ("test.png", NULL);
	crop = lm_crop_area_new (pix);

	gtk_widget_show (GTK_WIDGET (crop->canvas));
	gtk_container_add (GTK_CONTAINER (glade_xml_get_widget (xml, "scrolledwindow1")), GTK_WIDGET (crop->canvas));

	crop->xml = xml;
	w = crop->width;
	h = crop->height;

	set_spins_range (xml, crop, CROP_TOP, h, 0);
	set_spins_range (xml, crop, CROP_BOTTOM, h, h);
	set_spins_range (xml, crop, CROP_LEFT, w, 0);
	set_spins_range (xml, crop, CROP_RIGHT, w, w);
}

