
#include <glib.h>

typedef struct DVD DVD;
typedef struct AudioLang AudioLang;

typedef enum {
	FORMAT_NTSC,
	FORMAT_PAL,
} Format;

typedef enum {
	LANG_SUB,
	LANG_AUDIO,
} LangType;

typedef struct {
	int index;
	gint64 length;
	int num_chapters;
	int audio_langs;
	int subtitle_langs;
	Format format;
	int width, height;

	GList *subtitle;
	GList *audio;
	GList *chapters;
} DVDTrack;

typedef struct {
	int index;
	LangType type;
	char lang[2];
} Lang;

typedef struct {
	int index;
	gint64 length;
} Chapter;

DVD *dvd_new (const char *device);
void dvd_free (DVD *dvd);
const char *dvd_get_title_name (DVD *dvd);
GList *dvd_get_tracks (DVD *dvd);

