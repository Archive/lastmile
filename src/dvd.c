
#include <dvdread/ifo_read.h>
#include <sys/types.h>
#include <unistd.h>
#include "dvd.h"

#include <libintl.h>
#define _(String) gettext (String)
#ifdef gettext_noop
#   define N_(String) gettext_noop (String)
#else
#   define N_(String) (String)
#endif

static int video_height[2] = {480, 576};
static int video_width[4] = {720, 704, 352, 352};

struct DVD {
	char *device;
	char *title;
	dvd_reader_t *dvd;
	ifo_handle_t *ifo_zero, **ifo;
	int titles;

	GList *tracks;
};

static void
dvd_close (DVD *dvd)
{
	int i;

	if (dvd->dvd == NULL)
		return;

	if (dvd->ifo_zero != NULL) {
		for (i=1; i <= dvd->ifo_zero->vts_atrt->nr_of_vtss; i++) {
			ifoClose(dvd->ifo[i]);
		}

		ifoClose(dvd->ifo_zero);
	}

	DVDClose(dvd->dvd);
	dvd->dvd = NULL;
}

void
dvd_free (DVD *dvd)
{
	g_return_if_fail (dvd != NULL);

	dvd_close (dvd);

	g_free (dvd->device);
	g_free (dvd->title);

	g_free (dvd);
}

const char *
dvd_get_title_name (DVD *dvd)
{
	g_return_val_if_fail (dvd != NULL, NULL);

	return dvd->title;
}

static char *
dvd_get_title_name_helper (DVD *dvd)
{
	char title[33];
	int filehandle = 0;
	int last_char = 0, blah = 0;
	int bytes_read;

	if (!(filehandle = open(dvd->device, 00)))
	{
		return g_strdup (_("Unknown"));
	}

	if ( 32808 != lseek(filehandle, 32808, SEEK_SET )) {
		close(filehandle);
		return g_strdup (_("Unknown"));
	}

	if ( 32 != ( read(filehandle, title, 32))) {
		close(filehandle);
		return g_strdup (_("Unknown"));
	}

	close (filehandle);

	title[32] = '\0';
	for ( blah = 0; blah < 32; blah++ )
	{
		if (title[blah] != ' ')
			last_char = blah;
	}
	title[last_char+1] = '\0';

	return g_strdup (title);
}

static int
hex_to_dec (int hex)
{
	char *str;
	int dec;

	str = g_strdup_printf ("%02x", hex);
	dec = (int) g_strtod (str, NULL);
	g_free (str);

	return dec;
}

static GList *
dvd_get_audio (DVD *dvd, DVDTrack *track)
{
	vtsi_mat_t *vtsi_mat;
	GList *list;
	int i;

	g_return_val_if_fail (dvd != NULL, NULL);
	if (dvd->dvd == NULL)
		return NULL;

	vtsi_mat = dvd->ifo[dvd->ifo_zero->tt_srpt->title[track->index - 1].title_set_nr]->vtsi_mat;
	list = NULL;

	for (i = 0; i < vtsi_mat->nr_of_vts_audio_streams; i++)
	{
		audio_attr_t *audio_attr = &vtsi_mat->vts_audio_attr[i];
		Lang *lang;

		lang = g_new0 (Lang, 1);
		lang->index = i + 1;
		lang->type = LANG_AUDIO;
		sprintf(lang->lang, "%c%c", audio_attr->lang_code>>8, audio_attr->lang_code & 0xff);
		if (!lang->lang[0]) {
			lang->lang[0] = 'x';
			lang->lang[1] = 'x';
		}

		list = g_list_append (list, lang);
	}

	return list;
}

static GList *
dvd_get_subtitle (DVD *dvd, DVDTrack *track)
{
	vtsi_mat_t *vtsi_mat;
	GList *list;
	int i;

	g_return_val_if_fail (dvd != NULL, NULL);
	if (dvd->dvd == NULL)
		return NULL;

	vtsi_mat = dvd->ifo[dvd->ifo_zero->tt_srpt->title[track->index - 1].title_set_nr]->vtsi_mat;
	list = NULL;

	for (i = 0; i < vtsi_mat->nr_of_vts_subp_streams; i++)
	{
		subp_attr_t *subp_attr = &vtsi_mat->vts_subp_attr[i];
		Lang *lang;

		lang = g_new0 (Lang, 1);
		lang->index = i + 1;
		lang->type = LANG_SUB;
		sprintf(lang->lang, "%c%c", subp_attr->lang_code>>8,
				subp_attr->lang_code & 0xff);
		if (!lang->lang[0]) {
			lang->lang[0] = 'x';
			lang->lang[1] = 'x';
		}

		list = g_list_append (list, lang);
	}

	return list;
}

static GList *
dvd_get_chapters (DVD *dvd, DVDTrack *track)
{
	GList *list;
	int i, cell, vts_ttn, title_set_nr;
	pgc_t *pgc;
	pgcit_t *vts_pgcit;

	g_return_val_if_fail (dvd != NULL, NULL);
	if (dvd->dvd == NULL)
		return NULL;

	cell = 0;
	list = NULL;

	vts_pgcit  = dvd->ifo[dvd->ifo_zero->tt_srpt->title[track->index - 1].title_set_nr]->vts_pgcit;
	vts_ttn = dvd->ifo_zero->tt_srpt->title[track->index - 1].vts_ttn;
	title_set_nr = dvd->ifo_zero->tt_srpt->title[track->index - 1].title_set_nr;
	pgc = vts_pgcit->pgci_srp[dvd->ifo[title_set_nr]->vts_ptt_srpt->title[vts_ttn - 1].ptt[0].pgcn - 1].pgc;

	for (i=0; i < pgc->nr_of_programs; i++)
	{
		Chapter *chapter;
		int next;
		gint64 length = 0;

		chapter = g_new0 (Chapter, 1);

		if (i == pgc->nr_of_programs - 1)
			next = pgc->nr_of_cells + 1;
		else
			next = pgc->program_map[i+1];

		while (cell < next -1)
		{
			dvd_time_t *dvd_time;

			dvd_time = &pgc->cell_playback[cell].playback_time;
			length = hex_to_dec (dvd_time->hour) * 60 * 60
				+ hex_to_dec (dvd_time->minute) * 60
				+ hex_to_dec (dvd_time->second);
			cell++;
		}

		chapter->index = i + 1;
		chapter->length = length;
		list = g_list_append (list, chapter);
	}

	return list;
}

GList *
dvd_get_tracks (DVD *dvd)
{
	g_return_val_if_fail (dvd != NULL, NULL);
	return dvd->tracks;
}

static GList *
dvd_get_tracks_real (DVD *dvd)
{
	GList *list;
	dvd_time_t *dvd_time;
	pgcit_t *vts_pgcit;
	vtsi_mat_t *vtsi_mat;
	video_attr_t *video_attr;
	pgc_t *pgc;
	int title_set_nr, i, vts_ttn;

	g_return_val_if_fail (dvd != NULL, NULL);

	if (dvd->dvd == NULL)
		return NULL;

	list = NULL;

	for (i = 0; i < dvd->titles; i++) {
		DVDTrack *track;

		if (!(dvd->ifo[dvd->ifo_zero->tt_srpt->title[i].title_set_nr]->vtsi_mat))
			continue;

		track = g_new0 (DVDTrack, 1);

		vtsi_mat = dvd->ifo[dvd->ifo_zero->tt_srpt->title[i].title_set_nr]->vtsi_mat;
		vts_pgcit  = dvd->ifo[dvd->ifo_zero->tt_srpt->title[i].title_set_nr]->vts_pgcit;
		video_attr = &vtsi_mat->vts_video_attr;
		vts_ttn = dvd->ifo_zero->tt_srpt->title[i].vts_ttn;
		title_set_nr = dvd->ifo_zero->tt_srpt->title[i].title_set_nr;
		pgc = vts_pgcit->pgci_srp[dvd->ifo[title_set_nr]->vts_ptt_srpt->title[vts_ttn - 1].ptt[0].pgcn - 1].pgc;
		dvd_time = &pgc->playback_time;

		track->index = i + 1;
		track->length = hex_to_dec (dvd_time->hour) * 60 * 60
			+ hex_to_dec (dvd_time->minute) * 60
			+ hex_to_dec (dvd_time->second);
		track->num_chapters =
			dvd->ifo_zero->tt_srpt->title[i].nr_of_ptts;
		track->audio_langs = vtsi_mat->nr_of_vts_audio_streams;
		track->subtitle_langs = vtsi_mat->nr_of_vts_subp_streams;
		track->width = video_width[video_attr->picture_size];
		track->height = video_height[video_attr->video_format];
		track->format =  video_attr->video_format;

		track->subtitle = dvd_get_subtitle (dvd, track);
		track->audio = dvd_get_audio (dvd, track);
		track->chapters = dvd_get_chapters (dvd, track);

		list = g_list_append (list, track);
	}

	return list;
}

DVD *dvd_new (const char *device)
{
	DVD *dvd;
	int i;

	g_return_val_if_fail (device != NULL, NULL);

	dvd = g_new0 (DVD, 1);
	dvd->device = g_strdup (device);
	dvd->dvd = DVDOpen(device);

	if (dvd->dvd == NULL)
		return dvd;

	dvd->title = dvd_get_title_name_helper (dvd);

	dvd->ifo_zero = ifoOpen(dvd->dvd, 0);
	if (dvd->ifo_zero == NULL)
		return dvd;

	dvd->ifo = (ifo_handle_t **) g_new0 (ifo_handle_t,
			(dvd->ifo_zero->vts_atrt->nr_of_vtss + 1));
	for (i=1; i <= dvd->ifo_zero->vts_atrt->nr_of_vtss; i++) {
		dvd->ifo[i] = ifoOpen(dvd->dvd, i);
	}

	dvd->titles = dvd->ifo_zero->tt_srpt->nr_of_srpts;

	dvd->tracks = dvd_get_tracks_real (dvd);

	return dvd;
}

